import { Component, OnInit } from '@angular/core';
import { TodoApiService } from '../services/todo-api.service';
import { Todo } from '../models/todo.model';
import { TodoItemComponent } from '../todo-item/todo-item.component';

@Component({
  selector: 'app-todo-list',
  standalone: true,
  imports: [TodoItemComponent],
  templateUrl: './todo-list.component.html',
  styleUrl: './todo-list.component.css'
})
export class TodoListComponent implements OnInit {

  constructor(private _todoService: TodoApiService) {
  }

  todoList: Todo[] = []

  ngOnInit(): void {
    this._todoService.getAllTasks().subscribe((todos: Todo[]) => {
      this.todoList = todos
      console.log(this.todoList)
    })
  }

  onTodoDeleted(id: string): void {
    this.todoList = this.todoList.filter(todo => todo.id !== id);
  }
}
