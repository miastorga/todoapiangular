import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreateTodo, Todo } from '../models/todo.model';

@Injectable({
  providedIn: 'root'
})
export class TodoApiService {
  private baseURL = 'http://localhost:5114/api/tasks'

  constructor(private _httpClient: HttpClient) { }

  public getAllTasks(): Observable<Todo[]> {
    return this._httpClient.get<Todo[]>(`${this.baseURL}`)
  }

  public getTaskById(id: string): Observable<Todo> {
    return this._httpClient.get<Todo>(`${this.baseURL}/${id}`)
  }

  public deleteTaskById(id: string): Observable<void> {
    return this._httpClient.delete<void>(`${this.baseURL}/${id}`)
  }

  public createTask(todo: CreateTodo): Observable<Todo> {
    return this._httpClient.post<Todo>(`${this.baseURL}`, todo)
  }

  public updateTask(id: string, todo: Todo): Observable<Todo> {
    return this._httpClient.put<Todo>(`${this._httpClient}/${id}`, todo)
  }
}
