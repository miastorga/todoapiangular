import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoApiService } from '../app/services/todo-api.service';
import { Todo, CreateTodo } from '../app/models/todo.model';
import { FormsModule } from '@angular/forms';
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, TodoListComponent, FormsModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'TodoList';
  constructor(private _todoService: TodoApiService) {
  }
  newTodoDescription = ''
  errorMessage = ''

  createTodo() {
    if (this.newTodoDescription.trim() === '') {
      this.errorMessage = 'Ingresa una tarea valida';
      return;
    }

    const newTodo: CreateTodo = {
      descripcion: this.newTodoDescription,
      usuarioid: '10729c7d-2a4b-48c3-9da3-86c753da77f4'
    }

    this._todoService.createTask(newTodo).subscribe(() => {
      this.newTodoDescription = ''
      this.errorMessage = ''
    }, () => {
      this.errorMessage = 'Ocurio un error inesperado'
      this.errorMessage = ''
    });
  }
}
