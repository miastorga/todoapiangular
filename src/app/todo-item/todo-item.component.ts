import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TodoApiService } from '../services/todo-api.service';
import { Todo } from '../models/todo.model';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-todo-item',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './todo-item.component.html',
  styleUrl: './todo-item.component.css'
})
export class TodoItemComponent {
  constructor(private _todoService: TodoApiService) {
  }
  @Input() todo: Todo = {
    id: '',
    usuarioid: '',
    descripcion: '',
    fecha: new Date(),
  }

  @Output() todoDeleted = new EventEmitter<string>();
  @Output() todoUpdated = new EventEmitter<Todo>();

  isEditing = false;
  updatedDescription = this.todo.descripcion;

  getTodoById(id: string) {
    this._todoService.getTaskById(id).subscribe((todo: Todo) => {
      console.log(todo)
    })
  }

  deleteTodoById(id: string) {
    this._todoService.deleteTaskById(id).subscribe(() => {
      this.todoDeleted.emit(id);
    });
  }

  toggleEdit() {
    this.isEditing = !this.isEditing;
  }

  updateTodo() {
    const updatedTodo = { ...this.todo, descripcion: this.updatedDescription };
    this._todoService.updateTask(updatedTodo.id, updatedTodo).subscribe(todo => {
      this.todo = todo;
      this.todoUpdated.emit(todo);
      this.isEditing = false;
    });
  }
}
