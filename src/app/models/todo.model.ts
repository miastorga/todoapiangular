export type Todo = {
  id: string,
  usuarioid: string,
  descripcion: string,
  fecha: Date
}

export type CreateTodo = {
  descripcion: string,
  usuarioid: string
}