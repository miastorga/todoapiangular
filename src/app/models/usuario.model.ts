export type Usuario = {
  id: string,
  nombre: string,
  email: string
}